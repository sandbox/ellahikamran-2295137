Description
-----------
This module provide the functionality to view where an image is being referenced
provided that the images are being stored as a media content type with the
durpal site. For each image defined using a media content type the modules list
of all the places that image is being used
can be viewed by user.

Requirements
------------
Drupal 6.16 or higher

Installation
------------
1. Copy the entire image_referenced directory to
   Drupal sites/all/modules directory.

2. Login as an administrator. Enable the module in the "Administer" -> "Site
   Building" -> "Modules"

3. Create a content type media with the following field
   a. media_file , Field type = File and  widget type = Image.
   b. expiry_date, Field type = Date and widget type = Select list.


Support
-------
Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/image_referenced
