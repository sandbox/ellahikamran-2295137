<?php

/**
 * @file
 * Drush command for Image Referenced.
 */

/**
 * Implements hook_drush_command().
 */
function image_referenced_drush_command() {
  $commands = array();
  $commands['image-referenced-populate-table'] = array(
    'description' => "Populates the table 'image_usage'.",
    'examples' => array(
      'drush image-referenced-populate-table' => "Populates the table 'image_usage'.",
    ),
    'aliases' => array('irpt'),
  );

  return $commands;
}

/**
 * Populates the table 'image_usage'.
 */
function drush_image_referenced_populate_table() {
  $query = 'SELECT node.nid AS nid
            FROM {node} node
            WHERE node.type = "media";';
  $q = db_query($query);
  // Empties the table.
  db_query('TRUNCATE TABLE {' . IMAGE_REFERENCED_USAGE_TABLE . '};');
  while ($r = db_fetch_array($q)) {
    $results = _image_referenced_search_image_in_media_nodereference($r['nid']);
    if (count($results) > 0) {
      foreach ($results as $result) {
        $values[] = "(" . $result['nid'] . ", " . $r['nid'] . ", '" . $result['type_name'] . "', '" . $result['field_name'] . "')";
      }
    }
  }
  _image_referenced_insert_into_table($values);
}
