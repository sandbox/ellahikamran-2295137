<?php

/**
 * @file
 * Main functions for 'Image Referenced'.
 */

define('IMAGE_REFERENCED_USAGE_TABLE', 'image_usage');

/**
 * Implements hook_menu().
 */
function image_referenced_menu() {
  $items = array();

  $items['admin/content/image-referenced'] = array(
    'title' => 'Image Referenced',
    'page callback' => 'theme',
    'page arguments' => array('image_referenced_view'),
    'access arguments' => array('access  image referenced'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/content/image-referenced/%'] = array(
    'title' => 'Image Referenced Usage',
    'page callback' => 'theme',
    'page arguments' => array('image_referenced_usage', 3),
    'access arguments' => array('access image referenced'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implements hook_perm().
 */
function image_referenced_perm() {
  return array('access image referenced');
}

/**
 * Implements hook_theme().
 */
function image_referenced_theme() {
  $defaults = array(
    'file' => 'image_referenced.theme.inc',
  );
  return array(
    'image_referenced_view' => array(
      'arguments' => array(),
      'template' => 'themes/image-referenced-view',
    ) + $defaults,
    'image_referenced_usage' => array(
      'arguments' => array('img' => NULL),
      'template' => 'themes/image-referenced-usage',
    ) + $defaults,
  );
}

/**
 * Gets all the Media nodereference fields.
 *
 * @return array
 *   List of node reference fields.
 */
function _image_referenced_get_media_nodereference($node = NULL) {
  $rows = array();
  $cache_key = (!empty($node)) ? 'image_referenced_media_nodereference_' . $node->type : 'image_referenced_media_nodereference';
  $rows_cached = cache_get($cache_key);
  if (empty($rows_cached->data)) {
    $query = 'SELECT field.field_name AS field_name, field.global_settings AS global_settings,  field_instance.type_name AS type_name
    FROM {content_node_field} field  LEFT JOIN {content_node_field_instance} field_instance ON field.field_name = field_instance.field_name
    WHERE field.type = "nodereference" and field.global_settings REGEXP \'.+"referenceable_types";a:[0-9]+:{*.s:[0-9]+:"media";s:[0-9]+:"media".*\'';
    if (!empty($node)) {
      $query .= " and field_instance.type_name = '" . $node->type . "'";
    }
    $rs = db_query($query);
    while ($r = db_fetch_array($rs)) {
      $rows[] = $r;
    }
    cache_set($cache_key, $rows);
  }
  else {
    $rows = $rows_cached->data;
  }
  return $rows;
}

/**
 * Gets the nodes where an image is used.
 *
 * @param int $img
 *   Media node ID.
 *
 * @return array
 *   List of node ID.
 */
function _image_referenced_search_image_in_media_nodereference($img) {
  $rows = array();
  $nodes_ref = _image_referenced_get_media_nodereference();

  foreach ($nodes_ref as $node_ref) {
    $query = FALSE;
    if (db_table_exists('content_' . $node_ref['field_name'])) {
      $query = "SELECT content_field.nid AS nid
                FROM {content_" . $node_ref['field_name'] . "} content_field
                LEFT JOIN {node} node ON node.nid = content_field.nid
                RIGHT JOIN {workflow_current_revision} revision ON content_field.vid = revision.sid
                WHERE content_field." . $node_ref['field_name'] . "_nid = %d
                AND node.type = '%s'";
    }
    elseif (db_table_exists('content_type_' . $node_ref['type_name'])) {
      $query = "SELECT content_type.nid AS nid
                FROM {content_type_" . $node_ref['type_name'] . "} content_type
                LEFT JOIN {node} node ON node.nid = content_type.nid
                WHERE content_type." . $node_ref['field_name'] . "_nid = %d
                AND node.type = '%s'";
    }
    if (!empty($query)) {
      $q = db_query($query, $img, $node_ref['type_name']);
      while ($r = db_fetch_array($q)) {
        if (!in_array($r['nid'], $rows)) {
          $rows[] = array(
            'nid' => $r['nid'],
            'type_name' => $node_ref['type_name'],
            'field_name' => $node_ref['field_name'],
          );
        }
      }
    }
  }
  return $rows;
}

/**
 * Returns a date in a different format.
 *
 * @param string $expiry
 *   Date in the format %Y-%m-%dT%H:%M:%S.
 *
 * @return string
 *   Date in a new format:
 *   %d-%m-%Y (%y years, %d days, %h hours, %i minutes and %s seconds left).
 */
function _image_referenced_get_expiry_date_format($expiry) {
  // Converts date to timestamp.
  $format = '%Y-%m-%dT%H:%M:%S';
  $strf = strptime($expiry, $format);
  $date_str = $strf['tm_mday'] . '/' . ($strf['tm_mon'] + 1) . '/' . ($strf['tm_year'] + 1900);
  $now = new DateTime();
  $future_date = new DateTime($expiry);
  $interval = $future_date->diff($now);
  $time_left = '';
  $now_date = $now->format('Ymd');
  $expiry_date = $future_date->format('Ymd');
  // Checks if the expiry date is in the past.
  if ($now_date > $expiry_date) {
    $time_left .= t('Expired');
  }
  else {
    $year = $interval->y;
    $month = $interval->m;
    $day = $interval->d;
    $hour = $interval->h;
    if ($year > 1) {
      $time_left .= t('@year years', array('@year' => $year)) . ' ';
    }
    elseif ($year == 1) {
      $time_left .= t('@year year', array('@year' => $year)) . ' ';
    }

    if ($month > 1) {
      $time_left .= t('@month months', array('@month' => $month)) . ' ';
    }
    elseif ($month == 1) {
      $time_left .= t('@month month', array('@month' => $month)) . ' ';
    }

    if ($day > 1) {
      $time_left .= t('@day days', array('@day' => $day)) . ' ';
    }
    elseif ($day == 1) {
      $time_left .= t('@day day', array('@day' => $day)) . ' ';
    }

    if ($hour > 1) {
      $time_left .= t('@hour hours', array('@hour' => $hour)) . ' ';
    }
    elseif ($hour == 1) {
      $time_left .= t('@hour hour', array('@hour' => $hour)) . ' ';
    }
    $time_left .= t('left');
  }
  return $date_str . ' (' . $time_left . ')';
}

/**
 * Checks if a node is Media type.
 *
 * @param int $nid
 *   Node ID.
 *
 * @return bool
 *   True if it's a Media type.
 */
function _image_referenced_check_media_node($nid) {
  $node = node_load($nid);
  if (empty($node) || $node->type != 'media') {
    return FALSE;
  }
  return TRUE;
}

/**
 * Implements hook_nodeapi().
 *
 * Updates image_usage table every time a node
 * is created, updated and deleted.
 */
function image_referenced_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  $media_node_types = array(
    'bigmachinestable',
    'panel',
    'component',
    'page',
    'tool',
    'panel',
    'product',
    'promo_redesign',
    'richmedia',
    'video',
    'video_management',
  );
  if (in_array($node->type, variable_get('media_node_reference_node_types', $media_node_types))) {
    switch ($op) {
      case 'insert':
        if (isset($node->is_new) && $node->is_new == TRUE) {
          image_referenced_new_media_content($node);
        }
        break;

      case 'update':
      case 'delete':
      case 'delete revision':
        image_referenced_update_media_id_in_the_table($node, $op);
        break;

    }
  }
}

/**
 * Implements hook_form_alter().
 */
function image_referenced_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'rmg_admin_current_revision_workflow_tab_form') {
    $form['#submit'][] = 'image_referenced_revision_workflow_tab_form_submit';
  }
}

/**
 * Submit handler for the form on the workflow tab.
 *
 * @see workflow_tab_form
 */
function image_referenced_revision_workflow_tab_form_submit($form, &$form_state) {
  $node = $form_state['values']['node'];
  image_referenced_update_media_id_in_the_table($node);
}

/**
 * Updates a media content node in 'image_usage' table.
 *
 * @param object $node
 *   A full node.
 * @param string $op
 *   A full node operation.
 */
function image_referenced_update_media_id_in_the_table($node, $op = '') {
  // Deletes previous media_id for the node.
  $query = "DELETE FROM {" . IMAGE_REFERENCED_USAGE_TABLE . "} WHERE nid = %s";
  db_query($query, $node->nid);

  if ($op == 'update') {
    // Inserts info about $media_id.
    $values = _image_referenced_get_insert_values($node);
    _image_referenced_insert_into_table($values);
  }
}

/**
 * Inserts a media content node in 'image_usage' table.
 *
 * @param object $node
 *   A full node.
 */
function image_referenced_new_media_content($node) {
  $values = _image_referenced_get_insert_values($node);
  // Inserts info about $media_id.
  _image_referenced_insert_into_table($values);
}

/**
 * Inserts into images usage data in table.
 *
 * @param array $values
 *   An array of insert values.
 */
function _image_referenced_insert_into_table(array $values) {
  if (!empty($values)) {
    // Adds new info about $media_id.
    $values_string = (count($values) > 1) ? implode(",", $values) : $values[0];
    $query_insert = 'INSERT INTO {' . IMAGE_REFERENCED_USAGE_TABLE . '} (nid, media_id, type_name, field_name) VALUES ' . $values_string;
    db_query($query_insert);
  }
}

/**
 * Build image usage insert value list.
 *
 * @param object $node
 *   A full node.
 *
 * @return array
 *   A array of sql insert values.
 */
function _image_referenced_get_insert_values($node) {
  $nodes_ref = _image_referenced_get_media_nodereference($node);
  $values = array();
  foreach ($nodes_ref as $node_ref) {
    // If the content type has a media nodereference field.
    if ($node_ref['type_name'] == $node->type && isset($node->{$node_ref['field_name']})) {
      $image_field_value = $node->{$node_ref['field_name']};
      foreach ($image_field_value as $key => $value) {
        if (isset($image_field_value[$key]['nid']) && is_numeric($image_field_value[$key]['nid'])) {
          $values[] = "(" . $node->nid . ", " . $image_field_value[$key]['nid'] . ", '" . $node_ref['type_name'] . "', '" . $node_ref['field_name'] . "')";
        }
      }
    }
  }
  return $values;
}

/**
 * Gets the list of Image nodes.
 *
 * @param int|null $nid
 *   Image node ID.
 * @param array|null $header
 *   An array containing the table headers.
 * @param int $limit
 *   The number of query results to display per page.
 * @param int $pager_num
 *   An optional integer to distinguish between multiple pagers on one page.
 *
 * @return array
 *   A list of Image nodes.
 */
function _image_referenced_get_rows($nid = NULL, $header = NULL, $limit = 15, $pager_num = 0) {
  $rows = array();
  $query = 'SELECT node.nid AS nid,
            node.title AS title,
            node.created AS created,
            media.field_media_file_fid AS image_fid,
            media.field_expiry_date_value AS expiry,
            COUNT(count_usage.nid) AS count
            FROM {node} node
            LEFT JOIN {content_type_media} media ON node.nid = media.nid AND node.vid = media.vid
            RIGHT JOIN {' . IMAGE_REFERENCED_USAGE_TABLE . '} count_usage ON node.nid = count_usage.media_id
            WHERE node.type = "media"';

  // Filters by node ID.
  if (isset($nid)) {
    $query .= ' AND node.nid = ' . $nid;
  }
  $query .= ' GROUP BY nid ';
  if (isset($header)) {
    $query .= tablesort_sql($header);
  }

  $count_query = "SELECT COUNT(*) FROM (" . $query . ") AS count_query";
  $q = pager_query($query, $limit, $pager_num, $count_query);
  $attributes = array('width' => '30');
  while ($r = db_fetch_array($q)) {
    $row['nid'] = $r['nid'];
    $row['title'] = $r['title'];
    // Generates the thumbnail.
    $file = field_file_load($r['image_fid']);
    $file_path = $file['filepath'];
    $row['image'] = theme('image', $file_path, $r['title'], $r['title'], $attributes, FALSE);

    $row['created'] = format_date($r['created']);
    if (!empty($r['expiry'])) {
      $row['expiry'] = _image_referenced_get_expiry_date_format($r['expiry']);
    }
    else {
      $row['expiry'] = '-';
    }
    // Gets the nodes where the image is used.
    $row['count'] = $r['count'];

    $view_image = l(t('View'), 'node/' . $r['nid']);
    $edit_image = l(t('Edit'), 'node/' . $r['nid'] . '/edit');
    $view_usage = l(t('Usage'), 'admin/content/image-referenced/' . $r['nid']);
    if ($r['count'] > 0) {
      $row['operations'] = $view_image . ' | ' . $edit_image . ' | ' . $view_usage;
    }
    else {
      $row['operations'] = $view_image . ' | ' . $edit_image;
    }
    $rows[] = $row;
  }

  return $rows;
}

/**
 * Get all images for a content node from image usage table.
 *
 * @param object $nid
 *   A content node id.
 *
 * @return int
 *   Number of images used
 */
function _image_referenced_get_node_image($nid) {
  $count_image_used = FALSE;
  if (isset($nid)) {
    $query = 'select count(media_id) AS count_image_used from {' . IMAGE_REFERENCED_USAGE_TABLE . ' where nid = %s';
    $rs = db_query($query, $nid);
    if ($rs && ($row = db_fetch_array($rs))) {
      $count_image_used = $row['count_image_used'];
    }
    return $count_image_used;
  }
}
