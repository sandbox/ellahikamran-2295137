<?php

/**
 * @file
 * Preprocess functions for Image Referenced module.
 */

/**
 * Implements template_preprocess_hook().
 */
function template_preprocess_image_referenced_view(&$vars) {
  module_load_include('inc', 'image_referenced', 'includes/image_referenced_search_form.form');
  $vars['search'] = drupal_get_form('image_referenced_search_form');
  $header = array(
    0 => array('field' => 'nid', 'data' => t('Node ID'), 'sort' => 'desc'),
    1 => array('field' => 'title', 'data' => t('Title')),
    2 => array('data' => t('Small thumbnail')),
    3 => array('field' => 'created', 'data' => t('Created')),
    4 => array('field' => 'expiry', 'data' => t('Expiry date')),
    5 => array('field' => 'count', 'data' => t('Usage')),
    6 => array('data' => t('Operations')),
  );

  $nid = NULL;
  $pager_num = 0;
  $limit = 15;
  // Filters by node ID.
  if (isset($_GET['nid']) && _image_referenced_check_media_node($_GET['nid'])) {
    $nid = $_GET['nid'];
  }
  $rows = _image_referenced_get_rows($nid, $header, $limit, $pager_num);
  $vars['table'] = theme('table', $header, $rows);
  $vars['pager'] = theme('pager', NULL, $limit, $pager_num);
}

/**
 * Implements template_preprocess_hook().
 */
function template_preprocess_image_referenced_usage(&$vars) {
  $img_nid = $vars['img'];
  $media_node = node_load($img_nid);
  if (!empty($media_node) && $media_node->type == 'media') {
    $header = array(
      0 => array('data' => t('Node ID')),
      1 => array('data' => t('Title')),
      2 => array('data' => t('Type')),
      3 => array('data' => t('Created')),
      4 => array('data' => t('Operations')),
    );

    $query = 'SELECT node.nid AS nid
              FROM {' . IMAGE_REFERENCED_USAGE_TABLE . '} node
              WHERE node.media_id = %d';
    $q = db_query($query, $img_nid);
    $rows = array();
    while ($r = db_fetch_array($q)) {
      $node = node_load($r['nid']);
      $view = l(t('View'), 'node/' . $node->nid);
      $edit = l(t('Edit'), 'node/' . $node->nid . '/edit');
      $rows[] = array(
        'nid' => $node->nid,
        'title' => $node->title,
        'type' => $node->type,
        'created' => format_date($node->created),
        'operations' => $view . ' | ' . $edit,
      );
    }

    if (count($rows) > 0) {
      $vars['title_image'] = '<h2>' . t('Usage of @title - (@count)', array(
          '@title' => $media_node->title,
          '@count' => count($rows),
        )) . '</h2>';
      $vars['table'] = theme('table', $header, $rows);
    }
    else {
      $vars['title_image'] = '<h2>' . t('Usage of @title', array('@title' => $media_node->title)) . '</h2>';
      $vars['table'] = t('Image not used');
    }
  }
  else {
    $vars['title_image'] = '<h2>' . t('No media content selected') . '</h2>';
    $vars['table'] = '';
  }
  $vars['back'] = l(t('Go back'), 'admin/content/image-referenced');
}
