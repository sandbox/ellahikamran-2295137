<?php

/**
 * @file
 * Image Referenced search form.
 */

/**
 * Builder function for the search form.
 */
function image_referenced_search_form() {
  $default = (isset($_GET['nid'])) ? filter_xss($_GET['nid']) : '';
  // Defines a fieldset.
  $form['fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter'),
  );

  // Defines a textfield inside the fieldset.
  $form['fieldset']['search'] = array(
    '#type' => 'textfield',
    '#title' => t('Search by node ID'),
    '#description' => t('Enter the node ID.'),
    '#default_value' => $default,
    '#size' => 15,
  );

  // Defines a submit button.
  $form['fieldset']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

  return $form;
}

/**
 * Validate form function.
 */
function image_referenced_search_form_validate($form, &$form_state) {
  $nid = $form_state['values']['search'];
  if (!empty($nid) && !_imageferenced_check_media_node($nid)) {
    form_error($form['fieldset']['search'], t('Node ID not correct.'));
  }
}

/**
 * Submit form function.
 */
function image_referenced_search_form_submit($form, &$form_state) {
  $query = NULL;
  if ($form_state['values']['search'] != '') {
    $query = 'nid=' . $form_state['values']['search'];
  }
  drupal_goto('admin/content/image-referenced', $query);
}
