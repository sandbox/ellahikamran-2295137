<?php

/**
 * @file
 * Template file for Image Referenced usage page.
 */
?>
<div class="title">
  <?php print $title_image; ?>
</div>
<br/>
<div class="table">
  <?php print $table; ?>
</div>
<div class="back-link">
  <?php print $back; ?>
</div>
