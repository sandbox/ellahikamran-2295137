<?php

/**
 * @file
 * Template file for Image Referenced view page.
 */
?>
<div class="search">
  <?php print $search; ?>
</div>
<div class="table">
  <?php print $table; ?>
</div>
<div class="pager">
  <?php print $pager; ?>
</div>
